package com.codenjoy.dojo.client.highload;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.codenjoy.dojo.services.hash.Hash;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class Runner {

    private int iteration;
    private Map<String, List<Character>> status;
    private List<String> processed = new CopyOnWriteArrayList<>();

    public Runner(String host, int count) {
        status = new HashMap<>();

        int numLength = String.valueOf(count).length();

        for (int index = 1; index <= count; index++) {
            String number = StringUtils.leftPad(String.valueOf(index), numLength, "0");
            String name = "demo" + number + "@codenjoy.com";
            String code = Hash.getCode(name, name);
            String url = String.format("http://10.17.222.4/codenjoy-contest/board/player/af2bq9dfcbh07p4yye4x?code=55717544946947411");

            setupListener(name, url);
        }
    }

    private String move(char[][] board) {
        String[] string = new String[34];
        for (int i = 0; i < 33; i++) {
            string[33 - i] = new String(board[i]);
        }
        Coordinate userLocation = findUser(board);
        Coordinate bomb = checkBomb(userLocation, board);
        if (bomb==null)
            System.out.println("null");
        else
            System.out.println(bomb.toString());
        if (bomb != null) {
            return run(userLocation, board, bomb);
        }
        if (userLocation.x != 0 && userLocation.y != 0) {
            if (isEmptyPlace(new Coordinate(userLocation.x - 1, userLocation.y), board)) {
                return "(ACT, LEFT)";
            }
            if (isEmptyPlace(new Coordinate(userLocation.x, userLocation.y - 1), board)) {
                return "(ACT, DOWN)";
            }
            if (isEmptyPlace(new Coordinate(userLocation.x + 1, userLocation.y), board)) {
                return "(ACT, RIGHT)";
            }
            if (isEmptyPlace(new Coordinate(userLocation.x, userLocation.y + 1), board)) {
                return "(ACT, UP)";
            }
            if (isLoked(new Coordinate(userLocation.x, userLocation.y + 1), board)) {
                return "(ACT)";
            }
        }
        return "(ACT)";
    }

    boolean isLoked(Coordinate userLocation, char[][] board) {
        return notOutOfArray(userLocation) &&
                (board[userLocation.y + 1][userLocation.x] == '☼' || board[userLocation.y][userLocation.x] == '#'
                        && board[userLocation.y][userLocation.x + 1] == '☼' || board[userLocation.y][userLocation.x] == '#'
                        && board[userLocation.y - 1][userLocation.x] == '☼' || board[userLocation.y][userLocation.x] == '#'
                        && board[userLocation.y][userLocation.x - 1] == '☼' || board[userLocation.y][userLocation.x] == '#');
    }

    void createWay(Coordinate userLocation, char[][] board) {

    }

    boolean isCanAct(Coordinate userLocation, char[][] board) {
        return true;
    }

    boolean isEmpty(Coordinate userLocation, char[][] board){
        return notOutOfArray(userLocation) &&
                (board[userLocation.y][userLocation.x] == ' '
                        || board[userLocation.y][userLocation.x] == '҉'
                        || board[userLocation.y][userLocation.x] == 'H'
                        || board[userLocation.y][userLocation.x] == 'x'
                        || board[userLocation.y][userLocation.x] == '♣');
    }

    boolean isEmptyPlace(Coordinate userLocation, char[][] board) {
        return notOutOfArray(userLocation) &&
                (board[userLocation.y][userLocation.x] == ' '
                        || board[userLocation.y][userLocation.x] == '҉'
                        || board[userLocation.y][userLocation.x] == 'H'
                        || board[userLocation.y][userLocation.x] == 'x'
                        || board[userLocation.y][userLocation.x] == '♣')
                && checkBomb(new Coordinate(userLocation.x, userLocation.y), board) == null;
    }

    boolean notOutOfArray(Coordinate userLocation) {
        return userLocation.x > 0 && userLocation.x <= 32 && userLocation.y > 0 && userLocation.y <= 32;
    }

    Coordinate checkBomb(Coordinate userLocation, char[][] board) {
        Coordinate bomb = null;
        for (int i = userLocation.x - 4; i < userLocation.x ; i++) {
            if (i < 32 && i >= 1 && (isThisCharOnCoordinate( userLocation.y, '☻', board, i) || isThisCharOnCoordinate( userLocation.y, '♠', board, i) || isThisCharOnCoordinate( userLocation.y, '1', board, i)|| isThisCharOnCoordinate( userLocation.y, '2', board, i)|| isThisCharOnCoordinate( userLocation.y, '3', board, i)|| isThisCharOnCoordinate( userLocation.y, '4', board, i))) {
                bomb = new Coordinate(i, userLocation.y);
            }
            if (bomb != null && standSomething(board, new Coordinate(i, userLocation.y)))
            {
                bomb=null;
            }
        }
        if (bomb!=null)
            return bomb;

        for (int i = userLocation.x + 1 ; i <= userLocation.x + 4; i++) {
            if (bomb != null && standSomething(board, new Coordinate(i, userLocation.y)))
            {
                bomb = null;
            }
            if (i < 32 && i >= 1 && (isThisCharOnCoordinate( userLocation.y, '☻', board, i) || isThisCharOnCoordinate( userLocation.y, '♠', board, i) || isThisCharOnCoordinate( userLocation.y, '1', board, i)|| isThisCharOnCoordinate( userLocation.y, '2', board, i)|| isThisCharOnCoordinate( userLocation.y, '3', board, i)|| isThisCharOnCoordinate( userLocation.y, '4', board, i))) {
                return new Coordinate(i, userLocation.y);
            }
        }

        for (int i = userLocation.y - 4; i < userLocation.y ; i++) {
            if (i < 32 && i >= 1 && (isThisCharOnCoordinate(i, '☻', board, userLocation.x) || isThisCharOnCoordinate(i, '♠', board, userLocation.x)|| isThisCharOnCoordinate(i, '1', board, userLocation.x)|| isThisCharOnCoordinate(i, '2', board, userLocation.x)|| isThisCharOnCoordinate(i, '3', board, userLocation.x)|| isThisCharOnCoordinate(i, '4', board, userLocation.x)|| isThisCharOnCoordinate(i, '5', board, userLocation.x))) {
                bomb = new Coordinate(userLocation.x, i);
            }
            if (bomb != null && standSomething(board, new Coordinate(userLocation.x, i)))
            {
                bomb=null;
            }
        }
        if (bomb!=null)
            return bomb;
        for (int i = userLocation.y + 1; i <= userLocation.y + 4; i++) {
            if (bomb != null && standSomething(board, new Coordinate(userLocation.x, i)))
            {
                return null;
            }
            if (i < 32 && i >= 1 && (isThisCharOnCoordinate(i, '☻', board, userLocation.x) || isThisCharOnCoordinate(i, '♠', board, userLocation.x)|| isThisCharOnCoordinate(i, '1', board, userLocation.x)|| isThisCharOnCoordinate(i, '2', board, userLocation.x)|| isThisCharOnCoordinate(i, '3', board, userLocation.x)|| isThisCharOnCoordinate(i, '4', board, userLocation.x)|| isThisCharOnCoordinate(i, '5', board, userLocation.x))) {
                return new Coordinate(userLocation.x, i);
            }
        }

        return null;
    }

    public static void main(String[] args) {
        char[][] board = new Runner().createBoard("board=" +
                "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼" +
                "☼ #   # #  #♥#  #  #  &        #☼" +
                "☼♥☼♥☼♥☼#☼ ☼ ☼ ☼ ☼♥☼ ☼ ☼#☼#☼♥☼#☼#☼" +
                "☼#♥♥  ♥#   # #♥   # ♥#          ☼" +
                "☼ ☼ ☼#☼ ☼♥☼ ☼ ☼#☼ ☼ ☼ ☼ ☼&☼ ☼ ☼ ☼" +
                "☼     ♥          # #            ☼" +
                "☼ ☼ ☼ ☼ ☼♥☼ ☼ ☼♥☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼" +
                "☼#       # #     4       #  #  #☼" +
                "☼#☼♥☼ ☼#☼ ☼ ☼ ☼ ☼ ☼#☼ ☼ ☼#☼ ☼ ☼ ☼" +
                "☼#  # ♥#               # ♥   #  ☼" +
                "☼ ☼ ☼#☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼" +
                "☼   #♥ #      #                 ☼" +
                "☼ ☼ ☼ ☼ ☼♥☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼" +
                "☼     ## #     #   # #   ♥      ☼" +
                "☼ ☼ ☼♥☼ ☼ ☼#☼ ☼#☼ ☼ ☼♥☼ ☼ ☼ ☼ ☼ ☼" +
                "☼       #♥       #      ## # ###☼" +
                "☼ ☼ ☼ ☼#☼ ☼ ☼#☼ ☼ ☼#☼#☼&☼ ☼ ☼ ☼ ☼" +
                "☼       #       #    ♣# #     ♥ ☼" +
                "☼ ☼ ☼ ☼♥☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼" +
                "☼        ## ## ♥             # #☼" +
                "☼#☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼" +
                "☼4☺                 &    ###  ##☼" +
                "☼#☼#☼ ☼ ☼ ☼ ☼#☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼" +
                "☼                   ♥ ##        ☼" +
                "☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼♥☼#☼ ☼ ☼ ☼" +
                "☼     ##         &#         #   ☼" +
                "☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼" +
                "☼   #   #         #     # &     ☼" +
                "☼♥☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼#☼ ☼" +
                "☼  #                    ##   &  ☼" +
                "☼ ☼ ☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼#☼ ☼" +
                "☼ #    # &        #       #     ☼" +
                "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼");
        System.out.println(new Runner().move(board));
    }

    private boolean standSomething(char[][] board, Coordinate coordinate) {
        if (board[coordinate.y][coordinate.x] == '#'
        || board[coordinate.y][coordinate.x] == '☼'
        || board[coordinate.y][coordinate.x] == '&')
            return true;
        return false;
    }

    private boolean isThisCharOnCoordinate(int x, char c, char[][] board, int i) {
        return board[x][i] == c;
    }

    private Coordinate findUser(char[][] board) {
        Coordinate coordinate = new Coordinate();
        for (int y = 1; y < 32; y++) {
            for (int x = 1; x < 32; x++) {
                if (board[y][x] == '☺') {
                    coordinate.x = x;
                    coordinate.y = y;
                    return coordinate;
                }

            }
        }
        return coordinate;
    }

    public Runner() {
    }

    private char[][] createBoard(String data) {
        char[][] board = new char[33][33];

        for (int i = 0; i < 33; i++) {
            for (int k = 0; k < 33; k++) {
                board[32 - i][k] = data.charAt(i * 33 + k + 6);
            }
        }
        return board;
    }

    private void setupListener(String name, String url) {
        List<Character> statusLine = new LinkedList<>();
        status.put(name, statusLine);

        new WebSocketClient(url, new MySocket(name, statusLine));
    }

    String run(Coordinate userLocation, char[][] board, Coordinate bomb) {
//        втікаємо від бомби
//        повертаэмо якшо можемо

        if (isEmpty(new Coordinate(userLocation.x - 1, userLocation.y), board) && canRunFromBomb(new Coordinate(userLocation.x - 1, userLocation.y), board, bomb)) {
            return "(LEFT)";
        }
        if (isEmpty(new Coordinate(userLocation.x + 1, userLocation.y), board) && canRunFromBomb(new Coordinate(userLocation.x + 1, userLocation.y), board, bomb)) {
            return "(RIGHT)";
        }

        if (isEmpty(new Coordinate(userLocation.x, userLocation.y - 1), board) && canRunFromBomb(new Coordinate(userLocation.x, userLocation.y - 1), board, bomb)) {
            return "(DOWN)";
        }
        if (isEmpty(new Coordinate(userLocation.x, userLocation.y + 1), board) && canRunFromBomb(new Coordinate(userLocation.x, userLocation.y + 1), board, bomb)) {
            return "(UP)";
        }


        return null;
    }

    private boolean canRunFromBomb(Coordinate userLocation, char[][] board, Coordinate bomb) {
        if (board[bomb.y][bomb.x]-'0' == 1){
            if (isEmptyPlace(new Coordinate(userLocation.x - 1, userLocation.y), board)|| isEmptyPlace(new Coordinate(userLocation.x, userLocation.y - 1), board)||
            isEmptyPlace(new Coordinate(userLocation.x + 1, userLocation.y), board)||
            isEmptyPlace(new Coordinate(userLocation.x, userLocation.y + 1), board))
                return true;
            else
                return false;
        }else
            return canRun(userLocation, board, bomb);
    }

    boolean canRun(Coordinate userLocation, char[][] board, Coordinate bomb){
        board[bomb.y][bomb.x]--;
        String move = run(userLocation, board, bomb);
        if (move==null)
            return false;
        else
            return true;
    }

    @WebSocket
    public class MySocket {
        private final String name;
        private final List<Character> statusLine;
        private Session session;

        public MySocket(String name, List<Character> statusLine) {
            this.name = name;
            this.statusLine = statusLine;
        }

        @OnWebSocketConnect
        public void onConnect(Session session) {
            System.out.println(StringUtils.leftPad(name, 21) + "> !");
            this.session = session;
            statusLine.add('!');
        }

        @OnWebSocketClose
        public void onClose(int closeCode, String message) {
            statusLine.add('-');
        }

        @OnWebSocketError
        public void onError(Session session, Throwable reason) {
            statusLine.add('x');
        }

        @OnWebSocketMessage
        public void onMessage(String data) throws IOException {
            if (processed.contains(name)) {
                processed.clear();
                iteration++;

                status.entrySet().stream()
                        .sorted(Comparator.comparing(entry -> entry.getKey()))
                        .forEach(entry -> {
                            System.out.print(StringUtils.leftPad(entry.getKey(), 21) + "> ");
                            String status = entry.getValue().toString().replaceAll(", ", "").replaceAll("[\\[\\]]", "");
                            System.out.println(status);
                        });
                System.out.println("----------------------------------------------------------------------------------------------");
            }
            //створюємо дошку
            char[][] board = createBoard(data);

            //рухаємося


            processed.add(name);
            statusLine.add('+');


            session.getRemote().sendString(move(board));
        }
    }

}
